﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrackerLibrary;
using TrackerLibrary.Model;

namespace TrackerUI
{
    public partial class TournamentDashboardForm : Form
    {
        List<TournamentModel> tournaments = GlobalConfig.Connection.GetAllTournaments();

        public TournamentDashboardForm()
        {
            InitializeComponent();

            WireUpList();
        }

        private void WireUpList()
        {
            LoadExistingTournamentBox.DataSource = tournaments;
            LoadExistingTournamentBox.DisplayMember = "TournamentName";
        }

        private void CreateNewTournamentButton_Click(object sender, EventArgs e)
        {
            CreateTournamentForm form = new CreateTournamentForm();

            form.Show();
        }

        private void LoadTournamentButton_Click(object sender, EventArgs e)
        {
            TournamentModel model = (TournamentModel)LoadExistingTournamentBox.SelectedItem;

            TournamentViewerForm form = new TournamentViewerForm(model);

            form.Show();
        }
    }
}
