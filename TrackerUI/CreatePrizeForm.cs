﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrackerLibrary;
using TrackerLibrary.Model;
using TrackerLibrary.DataAccess;

namespace TrackerUI
{
    public partial class CreatePrizeForm : Form
    {
        InPrizeRequestor callingForm;


        public CreatePrizeForm(InPrizeRequestor caller)
        {
            InitializeComponent();

            callingForm = caller;
        }

        private void TeamNameValue_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void CreatePrizeButton_Click(object sender, EventArgs e)
        {
            if(ValidateForm())
            {
                // create new prize
                PrizeModel model = new PrizeModel(
                    PlaceNumberValue.Text, 
                    PlaceNameValue.Text, 
                    PrizeAmountValue.Text,
                    PrizePercentageValue.Text);

                // save new prize
                model = GlobalConfig.Connection.CreatePrize(model);

                callingForm.PrizeCompleted(model);

                this.Close();

            }
            else
            {
                MessageBox.Show("Form Invalid!");
            }
        }
        

        private bool ValidateForm()
        {

            bool output = true;

            int placeNumber = 1;
            bool PlaceNumberValidity = int.TryParse(PlaceNumberValue.Text, out placeNumber);

            decimal prizeAmount = 0;
            bool prizeAmountValidity = decimal.TryParse(PrizeAmountValue.Text, out prizeAmount);

            double prizePercentage = 0;
            bool prizePercentageValidity = double.TryParse(PrizePercentageValue.Text, out prizePercentage);


            if (!PlaceNumberValidity || !prizeAmountValidity || !prizePercentageValidity)
                output = false;

            if (placeNumber < 1)
                output = false;

            if (PlaceNameValue.Text.Length == 0)
                output = false;

            if (prizeAmount <= 0 && prizePercentage <= 0)
                output = false;

            if (prizePercentage > 100)
                output = false;


            return output;
        }
    }
}
