﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrackerLibrary.Model;

namespace TrackerUI
{
    public partial class TournamentViewerForm : Form
    {

        private TournamentModel tournament;

        private List<int> rounds = new List<int>();

        private List<MatchupModel> selectedMatchups = new List<MatchupModel>();


        public TournamentViewerForm(TournamentModel tournamentModel)
        {
            InitializeComponent();

            tournament = tournamentModel;

            LoadFormData();

            LoadRounds();
        }

        private void WireUpRounds()
        {
            SelectRoundBox.DataSource = null;
            SelectRoundBox.DataSource = rounds;
        }

        private void WireUpMatchups()
        {
            matchupListBox.DataSource = null;
            matchupListBox.DataSource = selectedMatchups;
            matchupListBox.DisplayMember = "DisplayName";
        }

        private void LoadFormData()
        {
            TournamentNameLabel.Text = tournament.TournamentName;
        }

        private void LoadRounds()
        {
            rounds.Add(1);
            int currentRound = 1;

            foreach(List<MatchupModel> matchups in tournament.Rounds)
            {
                if(matchups.First().MatchupRound > currentRound)
                {
                    currentRound = matchups.First().MatchupRound;
                    rounds.Add(currentRound);
                    currentRound++;
                }
            }

            WireUpRounds();
        }

        private void SelectRoundBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadMatchups();
        }

        private void LoadMatchups()
        {
            if(SelectRoundBox.SelectedItem != null)
            {
                int round = (int)SelectRoundBox.SelectedItem;

                foreach (List<MatchupModel> matchups in tournament.Rounds)
                {
                    if (matchups.First().MatchupRound == round)
                        selectedMatchups = matchups;
                }

                WireUpMatchups();
            }  
        }

        private void MatchupListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadMatchup();
        }

        private void LoadMatchup()
        {
            MatchupModel model = (MatchupModel)matchupListBox.SelectedItem;
            if (model != null)
            {
                for (int i = 0; i < model.Entries.Count; i++)
                {
                    if (i == 0)
                    {
                        if (model.Entries[0].TeamCompeting != null)
                        {
                            teamOneName.Text = model.Entries[0].TeamCompeting.TeamName;
                            TeamOneScoreValue.Text = model.Entries[0].Score.ToString();

                            teamTwoName.Text = "<bye>";
                            TeamTwoScoreValue.Text = "";
                        }
                        else
                        {
                            teamOneName.Text = "empty";
                            TeamOneScoreValue.Text = "";
                        }
                    }
                    if (i == 1)
                    {
                        if (model.Entries[1].TeamCompeting != null)
                        {
                            teamTwoName.Text = model.Entries[1].TeamCompeting.TeamName;
                            TeamTwoScoreValue.Text = model.Entries[1].Score.ToString();
                        }
                        else
                        {
                            teamTwoName.Text = "empty";
                            TeamTwoScoreValue.Text = "";
                        }
                    }
                }
            }
        }
        






        private void TournamentViewerForm_Load(object sender, EventArgs e)
        {

        }
        private void InitializeComponent()
        {
            this.HeaderLabel = new System.Windows.Forms.Label();
            this.TournamentName = new System.Windows.Forms.Label();
            this.RoundLabel = new System.Windows.Forms.Label();
            this.SelectRoundBox = new System.Windows.Forms.ComboBox();
            this.unplayedOnlyCheckBox = new System.Windows.Forms.CheckBox();
            this.matchupListBox = new System.Windows.Forms.ListBox();
            this.teamOneName = new System.Windows.Forms.Label();
            this.teamTwoName = new System.Windows.Forms.Label();
            this.TeamOneScoreLabel = new System.Windows.Forms.Label();
            this.TeamTwoScoreLabel = new System.Windows.Forms.Label();
            this.TeamOneScoreValue = new System.Windows.Forms.TextBox();
            this.TeamTwoScoreValue = new System.Windows.Forms.TextBox();
            this.VersusLabel = new System.Windows.Forms.Label();
            this.ScoreButton = new System.Windows.Forms.Button();
            this.TournamentNameLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // HeaderLabel
            // 
            this.HeaderLabel.AutoSize = true;
            this.HeaderLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HeaderLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(65)))));
            this.HeaderLabel.Location = new System.Drawing.Point(40, 28);
            this.HeaderLabel.Name = "HeaderLabel";
            this.HeaderLabel.Size = new System.Drawing.Size(168, 31);
            this.HeaderLabel.TabIndex = 0;
            this.HeaderLabel.Text = "Tournament:";
            // 
            // TournamentName
            // 
            this.TournamentName.AutoSize = true;
            this.TournamentName.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TournamentName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(65)))));
            this.TournamentName.Location = new System.Drawing.Point(216, 28);
            this.TournamentName.Name = "TournamentName";
            this.TournamentName.Size = new System.Drawing.Size(0, 31);
            this.TournamentName.TabIndex = 1;
            // 
            // RoundLabel
            // 
            this.RoundLabel.AutoSize = true;
            this.RoundLabel.Location = new System.Drawing.Point(42, 90);
            this.RoundLabel.Name = "RoundLabel";
            this.RoundLabel.Size = new System.Drawing.Size(78, 24);
            this.RoundLabel.TabIndex = 2;
            this.RoundLabel.Text = "Round:";
            this.RoundLabel.Click += new System.EventHandler(this.label1_Click);
            // 
            // SelectRoundBox
            // 
            this.SelectRoundBox.BackColor = System.Drawing.SystemColors.InfoText;
            this.SelectRoundBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SelectRoundBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(65)))));
            this.SelectRoundBox.FormattingEnabled = true;
            this.SelectRoundBox.Location = new System.Drawing.Point(130, 87);
            this.SelectRoundBox.Name = "SelectRoundBox";
            this.SelectRoundBox.Size = new System.Drawing.Size(121, 32);
            this.SelectRoundBox.TabIndex = 3;
            this.SelectRoundBox.SelectedIndexChanged += new System.EventHandler(this.SelectRoundBox_SelectedIndexChanged);
            // 
            // unplayedOnlyCheckBox
            // 
            this.unplayedOnlyCheckBox.AutoSize = true;
            this.unplayedOnlyCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.unplayedOnlyCheckBox.Location = new System.Drawing.Point(130, 138);
            this.unplayedOnlyCheckBox.Name = "unplayedOnlyCheckBox";
            this.unplayedOnlyCheckBox.Size = new System.Drawing.Size(157, 28);
            this.unplayedOnlyCheckBox.TabIndex = 4;
            this.unplayedOnlyCheckBox.Text = "unplayed only";
            this.unplayedOnlyCheckBox.UseVisualStyleBackColor = true;
            // 
            // matchupListBox
            // 
            this.matchupListBox.BackColor = System.Drawing.SystemColors.MenuText;
            this.matchupListBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(65)))));
            this.matchupListBox.FormattingEnabled = true;
            this.matchupListBox.ItemHeight = 24;
            this.matchupListBox.Location = new System.Drawing.Point(46, 202);
            this.matchupListBox.Name = "matchupListBox";
            this.matchupListBox.Size = new System.Drawing.Size(233, 148);
            this.matchupListBox.TabIndex = 5;
            this.matchupListBox.SelectedIndexChanged += new System.EventHandler(this.MatchupListBox_SelectedIndexChanged);
            // 
            // teamOneName
            // 
            this.teamOneName.AutoSize = true;
            this.teamOneName.Location = new System.Drawing.Point(327, 202);
            this.teamOneName.Name = "teamOneName";
            this.teamOneName.Size = new System.Drawing.Size(121, 24);
            this.teamOneName.TabIndex = 6;
            this.teamOneName.Text = "<team one>";
            // 
            // teamTwoName
            // 
            this.teamTwoName.AutoSize = true;
            this.teamTwoName.Location = new System.Drawing.Point(325, 331);
            this.teamTwoName.Name = "teamTwoName";
            this.teamTwoName.Size = new System.Drawing.Size(117, 24);
            this.teamTwoName.TabIndex = 7;
            this.teamTwoName.Text = "<team two>";
            this.teamTwoName.Click += new System.EventHandler(this.label1_Click_1);
            // 
            // TeamOneScoreLabel
            // 
            this.TeamOneScoreLabel.AutoSize = true;
            this.TeamOneScoreLabel.Location = new System.Drawing.Point(327, 235);
            this.TeamOneScoreLabel.Name = "TeamOneScoreLabel";
            this.TeamOneScoreLabel.Size = new System.Drawing.Size(68, 24);
            this.TeamOneScoreLabel.TabIndex = 8;
            this.TeamOneScoreLabel.Text = "score:";
            // 
            // TeamTwoScoreLabel
            // 
            this.TeamTwoScoreLabel.AutoSize = true;
            this.TeamTwoScoreLabel.Location = new System.Drawing.Point(325, 368);
            this.TeamTwoScoreLabel.Name = "TeamTwoScoreLabel";
            this.TeamTwoScoreLabel.Size = new System.Drawing.Size(68, 24);
            this.TeamTwoScoreLabel.TabIndex = 9;
            this.TeamTwoScoreLabel.Text = "score:";
            // 
            // TeamOneScoreValue
            // 
            this.TeamOneScoreValue.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.TeamOneScoreValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(80)))));
            this.TeamOneScoreValue.Location = new System.Drawing.Point(405, 235);
            this.TeamOneScoreValue.Name = "TeamOneScoreValue";
            this.TeamOneScoreValue.Size = new System.Drawing.Size(100, 29);
            this.TeamOneScoreValue.TabIndex = 10;
            // 
            // TeamTwoScoreValue
            // 
            this.TeamTwoScoreValue.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.TeamTwoScoreValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(80)))));
            this.TeamTwoScoreValue.Location = new System.Drawing.Point(403, 365);
            this.TeamTwoScoreValue.Name = "TeamTwoScoreValue";
            this.TeamTwoScoreValue.Size = new System.Drawing.Size(100, 29);
            this.TeamTwoScoreValue.TabIndex = 11;
            // 
            // VersusLabel
            // 
            this.VersusLabel.AutoSize = true;
            this.VersusLabel.Location = new System.Drawing.Point(351, 288);
            this.VersusLabel.Name = "VersusLabel";
            this.VersusLabel.Size = new System.Drawing.Size(36, 24);
            this.VersusLabel.TabIndex = 12;
            this.VersusLabel.Text = "vs.";
            // 
            // ScoreButton
            // 
            this.ScoreButton.BackColor = System.Drawing.Color.Black;
            this.ScoreButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ScoreButton.Location = new System.Drawing.Point(554, 277);
            this.ScoreButton.Name = "ScoreButton";
            this.ScoreButton.Size = new System.Drawing.Size(100, 45);
            this.ScoreButton.TabIndex = 13;
            this.ScoreButton.Text = "ACCEPT";
            this.ScoreButton.UseVisualStyleBackColor = false;
            // 
            // TournamentNameLabel
            // 
            this.TournamentNameLabel.AutoSize = true;
            this.TournamentNameLabel.Location = new System.Drawing.Point(218, 35);
            this.TournamentNameLabel.Name = "TournamentNameLabel";
            this.TournamentNameLabel.Size = new System.Drawing.Size(86, 24);
            this.TournamentNameLabel.TabIndex = 14;
            this.TournamentNameLabel.Text = "<name>";
            // 
            // TournamentViewerForm
            // 
            this.BackColor = System.Drawing.SystemColors.WindowText;
            this.ClientSize = new System.Drawing.Size(654, 461);
            this.Controls.Add(this.TournamentNameLabel);
            this.Controls.Add(this.ScoreButton);
            this.Controls.Add(this.VersusLabel);
            this.Controls.Add(this.TeamTwoScoreValue);
            this.Controls.Add(this.TeamOneScoreValue);
            this.Controls.Add(this.TeamTwoScoreLabel);
            this.Controls.Add(this.TeamOneScoreLabel);
            this.Controls.Add(this.teamTwoName);
            this.Controls.Add(this.teamOneName);
            this.Controls.Add(this.matchupListBox);
            this.Controls.Add(this.unplayedOnlyCheckBox);
            this.Controls.Add(this.SelectRoundBox);
            this.Controls.Add(this.RoundLabel);
            this.Controls.Add(this.TournamentName);
            this.Controls.Add(this.HeaderLabel);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(65)))));
            this.Name = "TournamentViewerForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds;
            this.Text = "Tournament Viewr";
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        private void label1_Click(object sender, EventArgs e)
        {

        }
        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        
    }
}
