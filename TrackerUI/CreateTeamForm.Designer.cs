﻿namespace TrackerUI
{
    partial class CreateTeamForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TeamNameValue = new System.Windows.Forms.TextBox();
            this.NameLabel = new System.Windows.Forms.Label();
            this.HeaderLabel = new System.Windows.Forms.Label();
            this.SelectMemberBox = new System.Windows.Forms.ComboBox();
            this.SelectMemberLabel = new System.Windows.Forms.Label();
            this.AddMemberButton = new System.Windows.Forms.Button();
            this.AddMemberBox = new System.Windows.Forms.GroupBox();
            this.CreateMemberButton = new System.Windows.Forms.Button();
            this.CellphoneValue = new System.Windows.Forms.TextBox();
            this.CellphoneLabel = new System.Windows.Forms.Label();
            this.EmailValue = new System.Windows.Forms.TextBox();
            this.EmailLabel = new System.Windows.Forms.Label();
            this.LastNameValue = new System.Windows.Forms.TextBox();
            this.LastNameLabel = new System.Windows.Forms.Label();
            this.FirstNameValue = new System.Windows.Forms.TextBox();
            this.FirstNameLabel = new System.Windows.Forms.Label();
            this.TeamPlayersListBox = new System.Windows.Forms.ListBox();
            this.RemoveSelectedMembersButton = new System.Windows.Forms.Button();
            this.CreateTeamButton = new System.Windows.Forms.Button();
            this.AddMemberBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // TeamNameValue
            // 
            this.TeamNameValue.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.TeamNameValue.ForeColor = System.Drawing.Color.Coral;
            this.TeamNameValue.Location = new System.Drawing.Point(197, 89);
            this.TeamNameValue.Margin = new System.Windows.Forms.Padding(6);
            this.TeamNameValue.Name = "TeamNameValue";
            this.TeamNameValue.Size = new System.Drawing.Size(196, 35);
            this.TeamNameValue.TabIndex = 14;
            // 
            // NameLabel
            // 
            this.NameLabel.AutoSize = true;
            this.NameLabel.Font = new System.Drawing.Font("3ds", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameLabel.Location = new System.Drawing.Point(17, 94);
            this.NameLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.NameLabel.Name = "NameLabel";
            this.NameLabel.Size = new System.Drawing.Size(66, 23);
            this.NameLabel.TabIndex = 13;
            this.NameLabel.Text = "Name:";
            // 
            // HeaderLabel
            // 
            this.HeaderLabel.AutoSize = true;
            this.HeaderLabel.Font = new System.Drawing.Font("3ds", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HeaderLabel.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.HeaderLabel.Location = new System.Drawing.Point(15, 20);
            this.HeaderLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.HeaderLabel.Name = "HeaderLabel";
            this.HeaderLabel.Size = new System.Drawing.Size(171, 31);
            this.HeaderLabel.TabIndex = 12;
            this.HeaderLabel.Text = "Create Team:";
            // 
            // SelectMemberBox
            // 
            this.SelectMemberBox.BackColor = System.Drawing.SystemColors.InfoText;
            this.SelectMemberBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SelectMemberBox.ForeColor = System.Drawing.Color.Aqua;
            this.SelectMemberBox.FormattingEnabled = true;
            this.SelectMemberBox.Location = new System.Drawing.Point(197, 150);
            this.SelectMemberBox.Name = "SelectMemberBox";
            this.SelectMemberBox.Size = new System.Drawing.Size(196, 33);
            this.SelectMemberBox.TabIndex = 17;
            // 
            // SelectMemberLabel
            // 
            this.SelectMemberLabel.AutoSize = true;
            this.SelectMemberLabel.Font = new System.Drawing.Font("3ds", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SelectMemberLabel.Location = new System.Drawing.Point(16, 153);
            this.SelectMemberLabel.Name = "SelectMemberLabel";
            this.SelectMemberLabel.Size = new System.Drawing.Size(166, 25);
            this.SelectMemberLabel.TabIndex = 16;
            this.SelectMemberLabel.Text = "Select Member:";
            // 
            // AddMemberButton
            // 
            this.AddMemberButton.BackColor = System.Drawing.Color.Black;
            this.AddMemberButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddMemberButton.Location = new System.Drawing.Point(197, 200);
            this.AddMemberButton.Name = "AddMemberButton";
            this.AddMemberButton.Size = new System.Drawing.Size(165, 45);
            this.AddMemberButton.TabIndex = 18;
            this.AddMemberButton.Text = "ADD MEMBER";
            this.AddMemberButton.UseVisualStyleBackColor = false;
            this.AddMemberButton.Click += new System.EventHandler(this.AddMemberButton_Click);
            // 
            // AddMemberBox
            // 
            this.AddMemberBox.BackColor = System.Drawing.Color.Transparent;
            this.AddMemberBox.Controls.Add(this.CreateMemberButton);
            this.AddMemberBox.Controls.Add(this.CellphoneValue);
            this.AddMemberBox.Controls.Add(this.CellphoneLabel);
            this.AddMemberBox.Controls.Add(this.EmailValue);
            this.AddMemberBox.Controls.Add(this.EmailLabel);
            this.AddMemberBox.Controls.Add(this.LastNameValue);
            this.AddMemberBox.Controls.Add(this.LastNameLabel);
            this.AddMemberBox.Controls.Add(this.FirstNameValue);
            this.AddMemberBox.Controls.Add(this.FirstNameLabel);
            this.AddMemberBox.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.AddMemberBox.Location = new System.Drawing.Point(21, 264);
            this.AddMemberBox.Name = "AddMemberBox";
            this.AddMemberBox.Size = new System.Drawing.Size(372, 279);
            this.AddMemberBox.TabIndex = 19;
            this.AddMemberBox.TabStop = false;
            this.AddMemberBox.Text = "Add New Member";
            // 
            // CreateMemberButton
            // 
            this.CreateMemberButton.BackColor = System.Drawing.Color.Black;
            this.CreateMemberButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CreateMemberButton.Font = new System.Drawing.Font("3ds", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CreateMemberButton.Location = new System.Drawing.Point(153, 216);
            this.CreateMemberButton.Name = "CreateMemberButton";
            this.CreateMemberButton.Size = new System.Drawing.Size(165, 45);
            this.CreateMemberButton.TabIndex = 19;
            this.CreateMemberButton.Text = "CREATE MEMBER";
            this.CreateMemberButton.UseVisualStyleBackColor = false;
            this.CreateMemberButton.Click += new System.EventHandler(this.CreateMemberButton_Click);
            // 
            // CellphoneValue
            // 
            this.CellphoneValue.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.CellphoneValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(80)))));
            this.CellphoneValue.Location = new System.Drawing.Point(153, 163);
            this.CellphoneValue.Name = "CellphoneValue";
            this.CellphoneValue.Size = new System.Drawing.Size(188, 35);
            this.CellphoneValue.TabIndex = 18;
            // 
            // CellphoneLabel
            // 
            this.CellphoneLabel.AutoSize = true;
            this.CellphoneLabel.Location = new System.Drawing.Point(13, 166);
            this.CellphoneLabel.Name = "CellphoneLabel";
            this.CellphoneLabel.Size = new System.Drawing.Size(113, 25);
            this.CellphoneLabel.TabIndex = 17;
            this.CellphoneLabel.Text = "Cellphone:";
            // 
            // EmailValue
            // 
            this.EmailValue.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.EmailValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(80)))));
            this.EmailValue.Location = new System.Drawing.Point(153, 122);
            this.EmailValue.Name = "EmailValue";
            this.EmailValue.Size = new System.Drawing.Size(188, 35);
            this.EmailValue.TabIndex = 16;
            // 
            // EmailLabel
            // 
            this.EmailLabel.AutoSize = true;
            this.EmailLabel.Location = new System.Drawing.Point(13, 125);
            this.EmailLabel.Name = "EmailLabel";
            this.EmailLabel.Size = new System.Drawing.Size(72, 25);
            this.EmailLabel.TabIndex = 15;
            this.EmailLabel.Text = "Email:";
            // 
            // LastNameValue
            // 
            this.LastNameValue.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.LastNameValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(80)))));
            this.LastNameValue.Location = new System.Drawing.Point(153, 81);
            this.LastNameValue.Name = "LastNameValue";
            this.LastNameValue.Size = new System.Drawing.Size(188, 35);
            this.LastNameValue.TabIndex = 14;
            // 
            // LastNameLabel
            // 
            this.LastNameLabel.AutoSize = true;
            this.LastNameLabel.Location = new System.Drawing.Point(13, 84);
            this.LastNameLabel.Name = "LastNameLabel";
            this.LastNameLabel.Size = new System.Drawing.Size(122, 25);
            this.LastNameLabel.TabIndex = 13;
            this.LastNameLabel.Text = "Last Name:";
            // 
            // FirstNameValue
            // 
            this.FirstNameValue.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.FirstNameValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(80)))));
            this.FirstNameValue.Location = new System.Drawing.Point(153, 40);
            this.FirstNameValue.Name = "FirstNameValue";
            this.FirstNameValue.Size = new System.Drawing.Size(188, 35);
            this.FirstNameValue.TabIndex = 12;
            // 
            // FirstNameLabel
            // 
            this.FirstNameLabel.AutoSize = true;
            this.FirstNameLabel.Location = new System.Drawing.Point(13, 43);
            this.FirstNameLabel.Name = "FirstNameLabel";
            this.FirstNameLabel.Size = new System.Drawing.Size(125, 25);
            this.FirstNameLabel.TabIndex = 11;
            this.FirstNameLabel.Text = "First Name:";
            // 
            // TeamPlayersListBox
            // 
            this.TeamPlayersListBox.BackColor = System.Drawing.SystemColors.MenuText;
            this.TeamPlayersListBox.ForeColor = System.Drawing.Color.DodgerBlue;
            this.TeamPlayersListBox.FormattingEnabled = true;
            this.TeamPlayersListBox.ItemHeight = 25;
            this.TeamPlayersListBox.Location = new System.Drawing.Point(484, 89);
            this.TeamPlayersListBox.Name = "TeamPlayersListBox";
            this.TeamPlayersListBox.Size = new System.Drawing.Size(297, 454);
            this.TeamPlayersListBox.TabIndex = 20;
            // 
            // RemoveSelectedMembersButton
            // 
            this.RemoveSelectedMembersButton.BackColor = System.Drawing.Color.Black;
            this.RemoveSelectedMembersButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RemoveSelectedMembersButton.Location = new System.Drawing.Point(808, 89);
            this.RemoveSelectedMembersButton.Name = "RemoveSelectedMembersButton";
            this.RemoveSelectedMembersButton.Size = new System.Drawing.Size(127, 107);
            this.RemoveSelectedMembersButton.TabIndex = 22;
            this.RemoveSelectedMembersButton.Text = "REMOVE SELECTED";
            this.RemoveSelectedMembersButton.UseVisualStyleBackColor = false;
            this.RemoveSelectedMembersButton.Click += new System.EventHandler(this.RemoveSelectedMembersButton_Click);
            // 
            // CreateTeamButton
            // 
            this.CreateTeamButton.BackColor = System.Drawing.Color.Black;
            this.CreateTeamButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CreateTeamButton.Font = new System.Drawing.Font("3ds", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CreateTeamButton.Location = new System.Drawing.Point(355, 565);
            this.CreateTeamButton.Name = "CreateTeamButton";
            this.CreateTeamButton.Size = new System.Drawing.Size(165, 45);
            this.CreateTeamButton.TabIndex = 23;
            this.CreateTeamButton.Text = "CREATE TEAM";
            this.CreateTeamButton.UseVisualStyleBackColor = false;
            this.CreateTeamButton.Click += new System.EventHandler(this.CreateTeamButton_Click);
            // 
            // CreateTeamForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(958, 634);
            this.Controls.Add(this.CreateTeamButton);
            this.Controls.Add(this.RemoveSelectedMembersButton);
            this.Controls.Add(this.TeamPlayersListBox);
            this.Controls.Add(this.AddMemberBox);
            this.Controls.Add(this.AddMemberButton);
            this.Controls.Add(this.SelectMemberBox);
            this.Controls.Add(this.SelectMemberLabel);
            this.Controls.Add(this.TeamNameValue);
            this.Controls.Add(this.NameLabel);
            this.Controls.Add(this.HeaderLabel);
            this.Font = new System.Drawing.Font("3ds", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "CreateTeamForm";
            this.Text = "Create Team";
            this.AddMemberBox.ResumeLayout(false);
            this.AddMemberBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TeamNameValue;
        private System.Windows.Forms.Label NameLabel;
        private System.Windows.Forms.Label HeaderLabel;
        private System.Windows.Forms.ComboBox SelectMemberBox;
        private System.Windows.Forms.Label SelectMemberLabel;
        private System.Windows.Forms.Button AddMemberButton;
        private System.Windows.Forms.GroupBox AddMemberBox;
        private System.Windows.Forms.TextBox FirstNameValue;
        private System.Windows.Forms.Label FirstNameLabel;
        private System.Windows.Forms.TextBox LastNameValue;
        private System.Windows.Forms.Label LastNameLabel;
        private System.Windows.Forms.Button CreateMemberButton;
        private System.Windows.Forms.TextBox CellphoneValue;
        private System.Windows.Forms.Label CellphoneLabel;
        private System.Windows.Forms.TextBox EmailValue;
        private System.Windows.Forms.Label EmailLabel;
        private System.Windows.Forms.ListBox TeamPlayersListBox;
        private System.Windows.Forms.Button RemoveSelectedMembersButton;
        private System.Windows.Forms.Button CreateTeamButton;
    }
}