﻿namespace TrackerUI
{
    partial class CreatePrizeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.HeaderLabel = new System.Windows.Forms.Label();
            this.PlaceNumberValue = new System.Windows.Forms.TextBox();
            this.PlaceNumberLabel = new System.Windows.Forms.Label();
            this.PlaceNameValue = new System.Windows.Forms.TextBox();
            this.PlaceNameLabel = new System.Windows.Forms.Label();
            this.PrizeAmountValue = new System.Windows.Forms.TextBox();
            this.PrizeAmountLabel = new System.Windows.Forms.Label();
            this.PrizePercentageValue = new System.Windows.Forms.TextBox();
            this.PricePercentageLabel = new System.Windows.Forms.Label();
            this.OrLabel = new System.Windows.Forms.Label();
            this.CreatePrizeButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // HeaderLabel
            // 
            this.HeaderLabel.AutoSize = true;
            this.HeaderLabel.Font = new System.Drawing.Font("3ds", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HeaderLabel.ForeColor = System.Drawing.Color.Azure;
            this.HeaderLabel.Location = new System.Drawing.Point(39, 30);
            this.HeaderLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.HeaderLabel.Name = "HeaderLabel";
            this.HeaderLabel.Size = new System.Drawing.Size(175, 33);
            this.HeaderLabel.TabIndex = 13;
            this.HeaderLabel.Text = "Create Prize:";
            // 
            // PlaceNumberValue
            // 
            this.PlaceNumberValue.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.PlaceNumberValue.ForeColor = System.Drawing.Color.Snow;
            this.PlaceNumberValue.HideSelection = false;
            this.PlaceNumberValue.Location = new System.Drawing.Point(207, 86);
            this.PlaceNumberValue.Margin = new System.Windows.Forms.Padding(6);
            this.PlaceNumberValue.Name = "PlaceNumberValue";
            this.PlaceNumberValue.Size = new System.Drawing.Size(196, 35);
            this.PlaceNumberValue.TabIndex = 16;
            this.PlaceNumberValue.TextChanged += new System.EventHandler(this.TeamNameValue_TextChanged);
            // 
            // PlaceNumberLabel
            // 
            this.PlaceNumberLabel.AutoSize = true;
            this.PlaceNumberLabel.Font = new System.Drawing.Font("3ds", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PlaceNumberLabel.Location = new System.Drawing.Point(42, 91);
            this.PlaceNumberLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.PlaceNumberLabel.Name = "PlaceNumberLabel";
            this.PlaceNumberLabel.Size = new System.Drawing.Size(136, 23);
            this.PlaceNumberLabel.TabIndex = 15;
            this.PlaceNumberLabel.Text = "Place Number:";
            // 
            // PlaceNameValue
            // 
            this.PlaceNameValue.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.PlaceNameValue.ForeColor = System.Drawing.Color.Snow;
            this.PlaceNameValue.HideSelection = false;
            this.PlaceNameValue.Location = new System.Drawing.Point(207, 151);
            this.PlaceNameValue.Margin = new System.Windows.Forms.Padding(6);
            this.PlaceNameValue.Name = "PlaceNameValue";
            this.PlaceNameValue.Size = new System.Drawing.Size(196, 35);
            this.PlaceNameValue.TabIndex = 18;
            // 
            // PlaceNameLabel
            // 
            this.PlaceNameLabel.AutoSize = true;
            this.PlaceNameLabel.Font = new System.Drawing.Font("3ds", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PlaceNameLabel.Location = new System.Drawing.Point(42, 156);
            this.PlaceNameLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.PlaceNameLabel.Name = "PlaceNameLabel";
            this.PlaceNameLabel.Size = new System.Drawing.Size(117, 23);
            this.PlaceNameLabel.TabIndex = 17;
            this.PlaceNameLabel.Text = "Place Name:";
            this.PlaceNameLabel.Click += new System.EventHandler(this.label1_Click);
            // 
            // PrizeAmountValue
            // 
            this.PrizeAmountValue.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.PrizeAmountValue.ForeColor = System.Drawing.Color.Snow;
            this.PrizeAmountValue.HideSelection = false;
            this.PrizeAmountValue.Location = new System.Drawing.Point(207, 217);
            this.PrizeAmountValue.Margin = new System.Windows.Forms.Padding(6);
            this.PrizeAmountValue.Name = "PrizeAmountValue";
            this.PrizeAmountValue.Size = new System.Drawing.Size(196, 35);
            this.PrizeAmountValue.TabIndex = 20;
            this.PrizeAmountValue.Text = "0";
            // 
            // PrizeAmountLabel
            // 
            this.PrizeAmountLabel.AutoSize = true;
            this.PrizeAmountLabel.Font = new System.Drawing.Font("3ds", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PrizeAmountLabel.Location = new System.Drawing.Point(42, 222);
            this.PrizeAmountLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.PrizeAmountLabel.Name = "PrizeAmountLabel";
            this.PrizeAmountLabel.Size = new System.Drawing.Size(134, 23);
            this.PrizeAmountLabel.TabIndex = 19;
            this.PrizeAmountLabel.Text = "Prize Amount:";
            // 
            // PrizePercentageValue
            // 
            this.PrizePercentageValue.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.PrizePercentageValue.ForeColor = System.Drawing.Color.Snow;
            this.PrizePercentageValue.HideSelection = false;
            this.PrizePercentageValue.Location = new System.Drawing.Point(207, 332);
            this.PrizePercentageValue.Margin = new System.Windows.Forms.Padding(6);
            this.PrizePercentageValue.Name = "PrizePercentageValue";
            this.PrizePercentageValue.Size = new System.Drawing.Size(196, 35);
            this.PrizePercentageValue.TabIndex = 22;
            this.PrizePercentageValue.Text = "0";
            // 
            // PricePercentageLabel
            // 
            this.PricePercentageLabel.AutoSize = true;
            this.PricePercentageLabel.Font = new System.Drawing.Font("3ds", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PricePercentageLabel.Location = new System.Drawing.Point(42, 337);
            this.PricePercentageLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.PricePercentageLabel.Name = "PricePercentageLabel";
            this.PricePercentageLabel.Size = new System.Drawing.Size(162, 23);
            this.PricePercentageLabel.TabIndex = 21;
            this.PricePercentageLabel.Text = "Prize Percentage:";
            // 
            // OrLabel
            // 
            this.OrLabel.AutoSize = true;
            this.OrLabel.Font = new System.Drawing.Font("3ds", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OrLabel.Location = new System.Drawing.Point(42, 280);
            this.OrLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.OrLabel.Name = "OrLabel";
            this.OrLabel.Size = new System.Drawing.Size(40, 23);
            this.OrLabel.TabIndex = 23;
            this.OrLabel.Text = "OR:";
            // 
            // CreatePrizeButton
            // 
            this.CreatePrizeButton.BackColor = System.Drawing.Color.Black;
            this.CreatePrizeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CreatePrizeButton.Font = new System.Drawing.Font("3ds", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CreatePrizeButton.Location = new System.Drawing.Point(207, 419);
            this.CreatePrizeButton.Name = "CreatePrizeButton";
            this.CreatePrizeButton.Size = new System.Drawing.Size(165, 45);
            this.CreatePrizeButton.TabIndex = 24;
            this.CreatePrizeButton.Text = "CREATE PRIZE";
            this.CreatePrizeButton.UseVisualStyleBackColor = false;
            this.CreatePrizeButton.Click += new System.EventHandler(this.CreatePrizeButton_Click);
            // 
            // CreatePrizeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(623, 548);
            this.Controls.Add(this.CreatePrizeButton);
            this.Controls.Add(this.OrLabel);
            this.Controls.Add(this.PrizePercentageValue);
            this.Controls.Add(this.PricePercentageLabel);
            this.Controls.Add(this.PrizeAmountValue);
            this.Controls.Add(this.PrizeAmountLabel);
            this.Controls.Add(this.PlaceNameValue);
            this.Controls.Add(this.PlaceNameLabel);
            this.Controls.Add(this.PlaceNumberValue);
            this.Controls.Add(this.PlaceNumberLabel);
            this.Controls.Add(this.HeaderLabel);
            this.Font = new System.Drawing.Font("3ds", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "CreatePrizeForm";
            this.Text = "Create Prize";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label HeaderLabel;
        private System.Windows.Forms.TextBox PlaceNumberValue;
        private System.Windows.Forms.Label PlaceNumberLabel;
        private System.Windows.Forms.TextBox PlaceNameValue;
        private System.Windows.Forms.Label PlaceNameLabel;
        private System.Windows.Forms.TextBox PrizeAmountValue;
        private System.Windows.Forms.Label PrizeAmountLabel;
        private System.Windows.Forms.TextBox PrizePercentageValue;
        private System.Windows.Forms.Label PricePercentageLabel;
        private System.Windows.Forms.Label OrLabel;
        private System.Windows.Forms.Button CreatePrizeButton;
    }
}