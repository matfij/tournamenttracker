﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrackerLibrary;
using TrackerLibrary.Model;

namespace TrackerUI
{
    public partial class CreateTeamForm : Form
    {

        private List<PersonModel> avilableTeamMembers = GlobalConfig.Connection.GetAllPerson();

        private List<PersonModel> selectedTeamMembers = new List<PersonModel>();

        private InTeamRequestor callingForm;

        public CreateTeamForm(InTeamRequestor caller)
        {
            InitializeComponent();

            WireUpLists();

            callingForm
                = caller;
        }
        


        private void WireUpLists()
        {
            // TODO - upgrade refreshing 
            SelectMemberBox.DataSource = null;
            TeamPlayersListBox.DataSource = null;

            SelectMemberBox.DataSource = avilableTeamMembers;
            SelectMemberBox.DisplayMember = "FullName";

            TeamPlayersListBox.DataSource = selectedTeamMembers;
            TeamPlayersListBox.DisplayMember = "FirstName";
        }


        private void CreateMemberButton_Click(object sender, EventArgs e)
        {
            if (ValidateForm())
            {
                PersonModel person = new PersonModel();

                person.FirstName = FirstNameValue.Text;
                person.LastName = LastNameValue.Text;
                person.EmailAddress = EmailValue.Text;
                person.CallphoneNumber = CellphoneValue.Text;

                person = GlobalConfig.Connection.CreatePerson(person);

                selectedTeamMembers.Add(person);
                WireUpLists();

                FirstNameValue.Text = "";
                LastNameValue.Text = "";
                EmailValue.Text = "";
                CellphoneValue.Text = "";
                
            }
            else
            {
                MessageBox.Show("illegal input");
            }
        }

        private bool ValidateForm()
        {
            // TODO - improve validation process

            if(FirstNameValue.Text.Length < 3)
            {
                return false;
            }
            else if(LastNameValue.Text.Length < 3)
            {
                return false;
            }
            else if (EmailValue.Text.Length < 3)
            {
                return false;
            }
            else if (CellphoneValue.Text.Length < 3)
            {
                return false;
            }


            return true;
        }


        private void AddMemberButton_Click(object sender, EventArgs e)
        {
            PersonModel model = (PersonModel)SelectMemberBox.SelectedItem;

            if (model != null)
            {
                avilableTeamMembers.Remove(model);

                selectedTeamMembers.Add(model);

                WireUpLists(); 
            }
        }
        


        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void RemoveSelectedMembersButton_Click(object sender, EventArgs e)
        {
            PersonModel model = (PersonModel)TeamPlayersListBox.SelectedItem;

            if (model != null)
            {
                avilableTeamMembers.Add(model);

                selectedTeamMembers.Remove(model);

                WireUpLists(); 
            }
        }


        private void CreateTeamButton_Click(object sender, EventArgs e)
        {
            TeamModel model = new TeamModel();

            model.TeamName = TeamNameValue.Text;
            model.TeamMembers = selectedTeamMembers;

            GlobalConfig.Connection.CreateTeam(model);

            callingForm.TeamCompleted(model);

            this.Close();
        }
    }
}
