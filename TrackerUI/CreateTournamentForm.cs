﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrackerLibrary;
using TrackerLibrary.Model;

namespace TrackerUI
{
    public partial class CreateTournamentForm : Form, InPrizeRequestor, InTeamRequestor
    {
        List<TeamModel> avilableTeams = GlobalConfig.Connection.GetAllTeams();
        List<TeamModel> selectedTeams = new List<TeamModel>();
        List<PrizeModel> selectedPrizes = new List<PrizeModel>();

        InPrizeRequestor callingForm;


        public CreateTournamentForm()
        {
            InitializeComponent();

            WireUpLists();
        }

        private void RoundLabel_Click(object sender, EventArgs e)
        {

        }

        private void TeamListLabel_Click(object sender, EventArgs e)
        {
            
        }


        private void WireUpLists()
        {
            SelectTeamBox.DataSource = null;
            SelectTeamBox.DataSource = avilableTeams;
            SelectTeamBox.DisplayMember = "TeamName";

            TournamentTeamsList.DataSource = null;
            TournamentTeamsList.DataSource = selectedTeams;
            TournamentTeamsList.DisplayMember = "TeamName";

            PrizesList.DataSource = null;
            PrizesList.DataSource = selectedPrizes;
            PrizesList.DisplayMember = "PlaceName";
        }


        private void AddTeamButton_Click(object sender, EventArgs e)
        {
            TeamModel model = (TeamModel) SelectTeamBox.SelectedItem;

            if (model != null)
            {
                avilableTeams.Remove(model);

                selectedTeams.Add(model);

                WireUpLists();
            }
        }


        private void CreatePrizeButton_Click(object sender, EventArgs e)
        {
            // call create prize form
            CreatePrizeForm form = new CreatePrizeForm(this);
            form.Show();
        }


        public void PrizeCompleted(PrizeModel model)
        {

            // get back created prize
            selectedPrizes.Add(model);

            // add it to prize list
            WireUpLists();
        }


        public void TeamCompleted(TeamModel model)
        {
            selectedTeams.Add(model);

            WireUpLists();
        }


        private void NewTeamLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            CreateTeamForm form = new CreateTeamForm(this);

            form.Show();
        }


        private void RemoveSelectedPlayersButton_Click(object sender, EventArgs e)
        {
            TeamModel model = (TeamModel)TournamentTeamsList.SelectedItem;

            if(model != null)
            {
                selectedTeams.Remove(model);

                avilableTeams.Add(model);

                WireUpLists();
            }
        }


        private void RemoveSelectedPrizesButton_Click(object sender, EventArgs e)
        {
            PrizeModel model = (PrizeModel)PrizesList.SelectedItem;

            if (model != null)
            {
                selectedPrizes.Remove(model);

                WireUpLists();
            }
        }



        private void CreateTournamentButton_Click(object sender, EventArgs e)
        {
            // input validation
            decimal fee = 0;
            bool feeState = decimal.TryParse(EntryFeeValue.Text, out fee);

            if(!feeState)
            {
                MessageBox.Show("unvalid entry fee!");

                return;
            }
            else
            {
                // create tournament entry
                TournamentModel tm = new TournamentModel();

                tm.TournamentName = TournamentNameValue.Text;
                tm.EntryFee = fee;
                tm.Prizes = selectedPrizes;
                tm.EnteredTeams = selectedTeams;

                // create matchups
                TournamentLogic.CreateRounds(tm);

                // save tournament to a database
                GlobalConfig.Connection.CreateTournament(tm);
                           
            }

        }
    }
}
