﻿namespace TrackerUI
{
    partial class TournamentDashboardForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.HeaderLabel = new System.Windows.Forms.Label();
            this.LoadExistingTournamentBox = new System.Windows.Forms.ComboBox();
            this.LoadExistingTournamentLabel = new System.Windows.Forms.Label();
            this.LoadTournamentButton = new System.Windows.Forms.Button();
            this.CreateNewTournamentButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // HeaderLabel
            // 
            this.HeaderLabel.AutoSize = true;
            this.HeaderLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HeaderLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(85)))));
            this.HeaderLabel.Location = new System.Drawing.Point(238, 57);
            this.HeaderLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.HeaderLabel.Name = "HeaderLabel";
            this.HeaderLabel.Size = new System.Drawing.Size(320, 31);
            this.HeaderLabel.TabIndex = 14;
            this.HeaderLabel.Text = "Tournament Dashboard";
            // 
            // LoadExistingTournamentBox
            // 
            this.LoadExistingTournamentBox.BackColor = System.Drawing.SystemColors.InfoText;
            this.LoadExistingTournamentBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LoadExistingTournamentBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(85)))));
            this.LoadExistingTournamentBox.FormattingEnabled = true;
            this.LoadExistingTournamentBox.Location = new System.Drawing.Point(292, 193);
            this.LoadExistingTournamentBox.Name = "LoadExistingTournamentBox";
            this.LoadExistingTournamentBox.Size = new System.Drawing.Size(196, 33);
            this.LoadExistingTournamentBox.TabIndex = 17;
            // 
            // LoadExistingTournamentLabel
            // 
            this.LoadExistingTournamentLabel.AutoSize = true;
            this.LoadExistingTournamentLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LoadExistingTournamentLabel.Location = new System.Drawing.Point(255, 155);
            this.LoadExistingTournamentLabel.Name = "LoadExistingTournamentLabel";
            this.LoadExistingTournamentLabel.Size = new System.Drawing.Size(294, 25);
            this.LoadExistingTournamentLabel.TabIndex = 16;
            this.LoadExistingTournamentLabel.Text = "Load Existing Tournament:";
            // 
            // LoadTournamentButton
            // 
            this.LoadTournamentButton.BackColor = System.Drawing.Color.Black;
            this.LoadTournamentButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LoadTournamentButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LoadTournamentButton.Location = new System.Drawing.Point(315, 232);
            this.LoadTournamentButton.Name = "LoadTournamentButton";
            this.LoadTournamentButton.Size = new System.Drawing.Size(152, 45);
            this.LoadTournamentButton.TabIndex = 25;
            this.LoadTournamentButton.Text = "LOAD";
            this.LoadTournamentButton.UseVisualStyleBackColor = false;
            this.LoadTournamentButton.Click += new System.EventHandler(this.LoadTournamentButton_Click);
            // 
            // CreateNewTournamentButton
            // 
            this.CreateNewTournamentButton.BackColor = System.Drawing.Color.Black;
            this.CreateNewTournamentButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CreateNewTournamentButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CreateNewTournamentButton.Location = new System.Drawing.Point(292, 332);
            this.CreateNewTournamentButton.Name = "CreateNewTournamentButton";
            this.CreateNewTournamentButton.Size = new System.Drawing.Size(202, 87);
            this.CreateNewTournamentButton.TabIndex = 26;
            this.CreateNewTournamentButton.Text = "CREATE NEW TOURNAMENT";
            this.CreateNewTournamentButton.UseVisualStyleBackColor = false;
            this.CreateNewTournamentButton.Click += new System.EventHandler(this.CreateNewTournamentButton_Click);
            // 
            // TournamentDashboardForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(792, 476);
            this.Controls.Add(this.CreateNewTournamentButton);
            this.Controls.Add(this.LoadTournamentButton);
            this.Controls.Add(this.LoadExistingTournamentBox);
            this.Controls.Add(this.LoadExistingTournamentLabel);
            this.Controls.Add(this.HeaderLabel);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(85)))));
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "TournamentDashboardForm";
            this.Text = "Tournamentm  Dashboard";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label HeaderLabel;
        private System.Windows.Forms.ComboBox LoadExistingTournamentBox;
        private System.Windows.Forms.Label LoadExistingTournamentLabel;
        private System.Windows.Forms.Button LoadTournamentButton;
        private System.Windows.Forms.Button CreateNewTournamentButton;
    }
}