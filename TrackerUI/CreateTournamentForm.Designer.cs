﻿namespace TrackerUI
{
    partial class CreateTournamentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.HeaderLabel = new System.Windows.Forms.Label();
            this.NameLabel = new System.Windows.Forms.Label();
            this.TournamentNameValue = new System.Windows.Forms.TextBox();
            this.EntryFeeValue = new System.Windows.Forms.TextBox();
            this.EntryFeeLabel = new System.Windows.Forms.Label();
            this.SelectTeamBox = new System.Windows.Forms.ComboBox();
            this.SelectTeamLabel = new System.Windows.Forms.Label();
            this.NewTeamLink = new System.Windows.Forms.LinkLabel();
            this.AddTeamButton = new System.Windows.Forms.Button();
            this.CreatePrizeButton = new System.Windows.Forms.Button();
            this.TournamentTeamsList = new System.Windows.Forms.ListBox();
            this.TeamListLabel = new System.Windows.Forms.Label();
            this.RemoveSelectedPlayersButton = new System.Windows.Forms.Button();
            this.RemoveSelectedPrizesButton = new System.Windows.Forms.Button();
            this.PrizesListLabel = new System.Windows.Forms.Label();
            this.PrizesList = new System.Windows.Forms.ListBox();
            this.CreateTournamentButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // HeaderLabel
            // 
            this.HeaderLabel.AutoSize = true;
            this.HeaderLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HeaderLabel.ForeColor = System.Drawing.Color.Coral;
            this.HeaderLabel.Location = new System.Drawing.Point(37, 56);
            this.HeaderLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.HeaderLabel.Name = "HeaderLabel";
            this.HeaderLabel.Size = new System.Drawing.Size(257, 31);
            this.HeaderLabel.TabIndex = 1;
            this.HeaderLabel.Text = "Create Tournament:";
            // 
            // NameLabel
            // 
            this.NameLabel.AutoSize = true;
            this.NameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameLabel.Location = new System.Drawing.Point(39, 125);
            this.NameLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.NameLabel.Name = "NameLabel";
            this.NameLabel.Size = new System.Drawing.Size(71, 24);
            this.NameLabel.TabIndex = 3;
            this.NameLabel.Text = "Name:";
            // 
            // TournamentNameValue
            // 
            this.TournamentNameValue.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.TournamentNameValue.ForeColor = System.Drawing.Color.Coral;
            this.TournamentNameValue.Location = new System.Drawing.Point(184, 120);
            this.TournamentNameValue.Margin = new System.Windows.Forms.Padding(6);
            this.TournamentNameValue.Name = "TournamentNameValue";
            this.TournamentNameValue.Size = new System.Drawing.Size(196, 31);
            this.TournamentNameValue.TabIndex = 11;
            // 
            // EntryFeeValue
            // 
            this.EntryFeeValue.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.EntryFeeValue.ForeColor = System.Drawing.Color.Coral;
            this.EntryFeeValue.Location = new System.Drawing.Point(184, 183);
            this.EntryFeeValue.Margin = new System.Windows.Forms.Padding(6);
            this.EntryFeeValue.Name = "EntryFeeValue";
            this.EntryFeeValue.Size = new System.Drawing.Size(196, 31);
            this.EntryFeeValue.TabIndex = 13;
            this.EntryFeeValue.Text = "0";
            // 
            // EntryFeeLabel
            // 
            this.EntryFeeLabel.AutoSize = true;
            this.EntryFeeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EntryFeeLabel.Location = new System.Drawing.Point(39, 188);
            this.EntryFeeLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.EntryFeeLabel.Name = "EntryFeeLabel";
            this.EntryFeeLabel.Size = new System.Drawing.Size(107, 24);
            this.EntryFeeLabel.TabIndex = 12;
            this.EntryFeeLabel.Text = "Entry Fee:";
            // 
            // SelectTeamBox
            // 
            this.SelectTeamBox.BackColor = System.Drawing.SystemColors.InfoText;
            this.SelectTeamBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SelectTeamBox.ForeColor = System.Drawing.Color.Coral;
            this.SelectTeamBox.FormattingEnabled = true;
            this.SelectTeamBox.Location = new System.Drawing.Point(184, 242);
            this.SelectTeamBox.Name = "SelectTeamBox";
            this.SelectTeamBox.Size = new System.Drawing.Size(196, 33);
            this.SelectTeamBox.TabIndex = 15;
            // 
            // SelectTeamLabel
            // 
            this.SelectTeamLabel.AutoSize = true;
            this.SelectTeamLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SelectTeamLabel.Location = new System.Drawing.Point(38, 245);
            this.SelectTeamLabel.Name = "SelectTeamLabel";
            this.SelectTeamLabel.Size = new System.Drawing.Size(150, 25);
            this.SelectTeamLabel.TabIndex = 14;
            this.SelectTeamLabel.Text = "Select Team:";
            this.SelectTeamLabel.Click += new System.EventHandler(this.RoundLabel_Click);
            // 
            // NewTeamLink
            // 
            this.NewTeamLink.AutoSize = true;
            this.NewTeamLink.Location = new System.Drawing.Point(38, 302);
            this.NewTeamLink.Name = "NewTeamLink";
            this.NewTeamLink.Size = new System.Drawing.Size(184, 25);
            this.NewTeamLink.TabIndex = 16;
            this.NewTeamLink.TabStop = true;
            this.NewTeamLink.Text = "Create New Team";
            this.NewTeamLink.VisitedLinkColor = System.Drawing.Color.CornflowerBlue;
            this.NewTeamLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.NewTeamLink_LinkClicked);
            // 
            // AddTeamButton
            // 
            this.AddTeamButton.BackColor = System.Drawing.Color.Black;
            this.AddTeamButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddTeamButton.Location = new System.Drawing.Point(43, 348);
            this.AddTeamButton.Name = "AddTeamButton";
            this.AddTeamButton.Size = new System.Drawing.Size(145, 45);
            this.AddTeamButton.TabIndex = 17;
            this.AddTeamButton.Text = "ADD TEAM";
            this.AddTeamButton.UseVisualStyleBackColor = false;
            this.AddTeamButton.Click += new System.EventHandler(this.AddTeamButton_Click);
            // 
            // CreatePrizeButton
            // 
            this.CreatePrizeButton.BackColor = System.Drawing.Color.Black;
            this.CreatePrizeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CreatePrizeButton.Location = new System.Drawing.Point(43, 409);
            this.CreatePrizeButton.Name = "CreatePrizeButton";
            this.CreatePrizeButton.Size = new System.Drawing.Size(145, 45);
            this.CreatePrizeButton.TabIndex = 18;
            this.CreatePrizeButton.Text = "ADD PRIZE";
            this.CreatePrizeButton.UseVisualStyleBackColor = false;
            this.CreatePrizeButton.Click += new System.EventHandler(this.CreatePrizeButton_Click);
            // 
            // TournamentTeamsList
            // 
            this.TournamentTeamsList.BackColor = System.Drawing.SystemColors.MenuText;
            this.TournamentTeamsList.ForeColor = System.Drawing.Color.Coral;
            this.TournamentTeamsList.FormattingEnabled = true;
            this.TournamentTeamsList.ItemHeight = 25;
            this.TournamentTeamsList.Location = new System.Drawing.Point(477, 114);
            this.TournamentTeamsList.Name = "TournamentTeamsList";
            this.TournamentTeamsList.Size = new System.Drawing.Size(297, 179);
            this.TournamentTeamsList.TabIndex = 19;
            // 
            // TeamListLabel
            // 
            this.TeamListLabel.AutoSize = true;
            this.TeamListLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TeamListLabel.Location = new System.Drawing.Point(473, 77);
            this.TeamListLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.TeamListLabel.Name = "TeamListLabel";
            this.TeamListLabel.Size = new System.Drawing.Size(143, 24);
            this.TeamListLabel.TabIndex = 20;
            this.TeamListLabel.Text = "Team/Players:";
            this.TeamListLabel.Click += new System.EventHandler(this.TeamListLabel_Click);
            // 
            // RemoveSelectedPlayersButton
            // 
            this.RemoveSelectedPlayersButton.BackColor = System.Drawing.Color.Black;
            this.RemoveSelectedPlayersButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RemoveSelectedPlayersButton.Location = new System.Drawing.Point(799, 114);
            this.RemoveSelectedPlayersButton.Name = "RemoveSelectedPlayersButton";
            this.RemoveSelectedPlayersButton.Size = new System.Drawing.Size(216, 45);
            this.RemoveSelectedPlayersButton.TabIndex = 21;
            this.RemoveSelectedPlayersButton.Text = "REMOVE SELECTED";
            this.RemoveSelectedPlayersButton.UseVisualStyleBackColor = false;
            this.RemoveSelectedPlayersButton.Click += new System.EventHandler(this.RemoveSelectedPlayersButton_Click);
            // 
            // RemoveSelectedPrizesButton
            // 
            this.RemoveSelectedPrizesButton.BackColor = System.Drawing.Color.Black;
            this.RemoveSelectedPrizesButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RemoveSelectedPrizesButton.Location = new System.Drawing.Point(799, 370);
            this.RemoveSelectedPrizesButton.Name = "RemoveSelectedPrizesButton";
            this.RemoveSelectedPrizesButton.Size = new System.Drawing.Size(216, 45);
            this.RemoveSelectedPrizesButton.TabIndex = 24;
            this.RemoveSelectedPrizesButton.Text = "REMOVE SELECTED";
            this.RemoveSelectedPrizesButton.UseVisualStyleBackColor = false;
            this.RemoveSelectedPrizesButton.Click += new System.EventHandler(this.RemoveSelectedPrizesButton_Click);
            // 
            // PrizesListLabel
            // 
            this.PrizesListLabel.AutoSize = true;
            this.PrizesListLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PrizesListLabel.Location = new System.Drawing.Point(473, 333);
            this.PrizesListLabel.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.PrizesListLabel.Name = "PrizesListLabel";
            this.PrizesListLabel.Size = new System.Drawing.Size(73, 24);
            this.PrizesListLabel.TabIndex = 23;
            this.PrizesListLabel.Text = "Prizes:";
            // 
            // PrizesList
            // 
            this.PrizesList.BackColor = System.Drawing.SystemColors.MenuText;
            this.PrizesList.ForeColor = System.Drawing.Color.Coral;
            this.PrizesList.FormattingEnabled = true;
            this.PrizesList.ItemHeight = 25;
            this.PrizesList.Location = new System.Drawing.Point(477, 370);
            this.PrizesList.Name = "PrizesList";
            this.PrizesList.Size = new System.Drawing.Size(297, 179);
            this.PrizesList.TabIndex = 22;
            // 
            // CreateTournamentButton
            // 
            this.CreateTournamentButton.BackColor = System.Drawing.Color.Black;
            this.CreateTournamentButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CreateTournamentButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CreateTournamentButton.Location = new System.Drawing.Point(319, 584);
            this.CreateTournamentButton.Name = "CreateTournamentButton";
            this.CreateTournamentButton.Size = new System.Drawing.Size(288, 45);
            this.CreateTournamentButton.TabIndex = 25;
            this.CreateTournamentButton.Text = "CREATE TOURNAMENT";
            this.CreateTournamentButton.UseVisualStyleBackColor = false;
            this.CreateTournamentButton.Click += new System.EventHandler(this.CreateTournamentButton_Click);
            // 
            // CreateTournamentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(1045, 656);
            this.Controls.Add(this.CreateTournamentButton);
            this.Controls.Add(this.RemoveSelectedPrizesButton);
            this.Controls.Add(this.PrizesListLabel);
            this.Controls.Add(this.PrizesList);
            this.Controls.Add(this.RemoveSelectedPlayersButton);
            this.Controls.Add(this.TeamListLabel);
            this.Controls.Add(this.TournamentTeamsList);
            this.Controls.Add(this.CreatePrizeButton);
            this.Controls.Add(this.AddTeamButton);
            this.Controls.Add(this.NewTeamLink);
            this.Controls.Add(this.SelectTeamBox);
            this.Controls.Add(this.SelectTeamLabel);
            this.Controls.Add(this.EntryFeeValue);
            this.Controls.Add(this.EntryFeeLabel);
            this.Controls.Add(this.TournamentNameValue);
            this.Controls.Add(this.NameLabel);
            this.Controls.Add(this.HeaderLabel);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Coral;
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "CreateTournamentForm";
            this.Text = "Create Tournament";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label HeaderLabel;
        private System.Windows.Forms.Label NameLabel;
        private System.Windows.Forms.TextBox TournamentNameValue;
        private System.Windows.Forms.TextBox EntryFeeValue;
        private System.Windows.Forms.Label EntryFeeLabel;
        private System.Windows.Forms.ComboBox SelectTeamBox;
        private System.Windows.Forms.Label SelectTeamLabel;
        private System.Windows.Forms.LinkLabel NewTeamLink;
        private System.Windows.Forms.Button AddTeamButton;
        private System.Windows.Forms.Button CreatePrizeButton;
        private System.Windows.Forms.ListBox TournamentTeamsList;
        private System.Windows.Forms.Label TeamListLabel;
        private System.Windows.Forms.Button RemoveSelectedPlayersButton;
        private System.Windows.Forms.Button RemoveSelectedPrizesButton;
        private System.Windows.Forms.Label PrizesListLabel;
        private System.Windows.Forms.ListBox PrizesList;
        private System.Windows.Forms.Button CreateTournamentButton;
    }
}