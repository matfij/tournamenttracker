﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrackerLibrary.Model
{
    /// <summary>
    /// Represents a tournament prize money.
    /// </summary>
    public class PrizeModel
    {

        public int ID { get; set; }

        public int PlaceNumber { get; set; }

        public string PlaceName { get; set; }

        public decimal PrizeAmount { get; set; }

        public double PrizePercentage { get; set; }


        public PrizeModel()
        {

        }

        public PrizeModel(string placeNumber, string placeName, string prizeAmount, string prizePercentage)
        {

            this.PlaceNumber = int.Parse(placeNumber);

            this.PlaceName = placeName;

            this.PrizeAmount = decimal.Parse(prizeAmount);

            this.PrizePercentage = double.Parse(prizePercentage);
        }
    }
}
