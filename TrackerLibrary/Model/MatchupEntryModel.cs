﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrackerLibrary.Model
{
    /// <summary>
    /// Represents a single matchup entry in a tournament.
    /// </summary>
    public class MatchupEntryModel
    {
        public int ID { get; set; }

        public int TeamCompetingID { get; set; }

        public TeamModel TeamCompeting { get; set; }

        public double Score { get; set; }

        public int ParenMatchupID { get; set; }

        public MatchupModel ParenMatchup { get; set; }

    }
}
