﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrackerLibrary.Model
{
    /// <summary>
    /// Represents a single matchup in a tournament.
    /// </summary>
    public class MatchupModel
    {
        public int ID { get; set; }

        public List<MatchupEntryModel> Entries { get; set; } = new List<MatchupEntryModel>();

        public int WinnerID { get; set; }

        public TeamModel Winner { get; set; }

        public int MatchupRound { get; set; }

        public string DisplayName
        {
            get
            {
                string output = "";

                foreach (MatchupEntryModel mem in Entries)
                {
                    if (mem.TeamCompeting != null)
                    {
                        if (output.Length == 0)
                            output = mem.TeamCompeting.TeamName;
                        else
                            output += $" vs {mem.TeamCompeting.TeamName}";
                    }
                    else
                    {
                        output = "Matchup not yet determined.";
                        break;
                    }
                }

                return output;
            }
        }
    }
}
