﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrackerLibrary.Model
{
    /// <summary>
    /// Represents a single competitor in a team.
    /// </summary>
    public class PersonModel
    {
        public int ID { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string EmailAddress { get; set; }

        public string CallphoneNumber { get; set; }

        public string FullName
        {
            get { return $"{FirstName} {LastName}"; }
        } 
    }
}
