﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrackerLibrary.Model;

namespace TrackerLibrary
{
    public static class TournamentLogic
    {

        public static void CreateRounds(TournamentModel model)
        {
            // order list<teams> randomly
            List<TeamModel> randomizedTeams = RandomizeTeamOrder(model.EnteredTeams);

            // check if it is big enough - 2^n, if needed add a byes
            int rounds = FindNumberOfRounds(randomizedTeams);

            int byes = FindNumberOfByes(randomizedTeams, rounds);

            // create first round of matchups
            model.Rounds.Add(CreateFirstRound(byes, randomizedTeams));

            // create vessel for every other round
            CreateOtherRounds(model, rounds);
        }


        private static void CreateOtherRounds(TournamentModel model, int rounds)
        {
            int round = 2;
            List<MatchupModel> previousRound = model.Rounds[0];
            List<MatchupModel> currentRound = new List<MatchupModel>();
            MatchupModel currentMatchup = new MatchupModel();

            while(round <= rounds)
            {
                foreach(MatchupModel match in previousRound)
                {
                    currentMatchup.Entries.Add(new MatchupEntryModel { ParenMatchup = match });

                    if(currentMatchup.Entries.Count > 1)
                    {
                        currentMatchup.MatchupRound = round;
                        currentRound.Add(currentMatchup);
                        currentMatchup = new MatchupModel();
                    }
                }

                model.Rounds.Add(currentRound);

                previousRound = currentRound;
                currentRound = new List<MatchupModel>();
                round++;
            }
        }


        private static List<MatchupModel> CreateFirstRound(int byes, List<TeamModel> teamList)
        {

            List<MatchupModel> output = new List<MatchupModel>();
            MatchupModel tempMatchup = new MatchupModel();

            foreach(TeamModel team in teamList)
            {
                tempMatchup.Entries.Add(new MatchupEntryModel {TeamCompeting = team});

                if(byes > 0 || tempMatchup.Entries.Count > 1)
                {
                    tempMatchup.MatchupRound = 1;
                    output.Add(tempMatchup);
                    tempMatchup = new MatchupModel();

                    if (byes > 0)
                        byes--;
                }
            }

            return output;
        }


        private static int FindNumberOfByes(List<TeamModel> teamList, int rounds)
        {
            int output = 0;
            int teamCount = teamList.Count;

            output = rounds*rounds - teamCount;

            return output;
        }


        private static int FindNumberOfRounds(List<TeamModel> teamList)
        {
            int output = 1;
            int teamCount = teamList.Count;
            int val = 2;

            while(val < teamCount)
            {
                val *= 2;
                output ++;
            }

            return output;
        }


        private static List<TeamModel> RandomizeTeamOrder(List<TeamModel> teamList)
        {
            teamList = teamList.OrderBy(a => Guid.NewGuid()).ToList();

            return teamList;
        }


    }
}
