﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrackerLibrary.DataAccess;
using System.Configuration;


namespace TrackerLibrary
{
    public static class GlobalConfig
    {

        /// <summary>
        /// the type of a connection to some data source
        /// </summary>
        public static InDataConnection Connection { get; private set; } 


        /// <summary>
        /// opens a new sql connection
        /// </summary>
        public static void InitializeConnections(DatabaseType type)
        {
            switch(type)
            {
                case DatabaseType.Sql:

                    // initialize the SQL connection
                    SqlConnector sql = new SqlConnector();
                    Connection = sql;
                    break;

                default:
                    break;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        public static string GetConnectionString(string name)
        {
            return ConfigurationManager.ConnectionStrings[name].ConnectionString;
        }
    }
}
