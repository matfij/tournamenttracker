﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrackerLibrary.Model;


namespace TrackerLibrary.DataAccess
{
    /// <summary>
    /// Interface - includes all required methods to intearct with DB via SqlConnector
    /// </summary>
    public interface InDataConnection
    {

        PrizeModel CreatePrize(PrizeModel model);

        PersonModel CreatePerson(PersonModel model);
        
        TeamModel CreateTeam(TeamModel model);

        TournamentModel CreateTournament(TournamentModel model);

        List<PersonModel> GetAllPerson();

        List<TournamentModel> GetAllTournaments();

        List<TeamModel> GetAllTeams();
    }
}
