﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrackerLibrary.Model;
using Dapper;


namespace TrackerLibrary.DataAccess
{
    /// <summary>
    /// interface between Data Base and C Sharp desktop User Interface
    /// </summary>
    class SqlConnector : InDataConnection
    {

        /// <summary>
        /// saves a new price to the database
        /// </summary>
        public PrizeModel CreatePrize(PrizeModel model)
        {

            using (IDbConnection connection =
                new System.Data.SqlClient.SqlConnection(GlobalConfig.GetConnectionString("Tournaments")))
            {
                var prizeParameters = new DynamicParameters();

                prizeParameters.Add("@PlaceNumber", model.PlaceNumber);
                prizeParameters.Add("@PlaceName", model.PlaceName);
                prizeParameters.Add("@PrizeAmount", model.PrizeAmount);
                prizeParameters.Add("@PrizePercentage", model.PrizePercentage);
                prizeParameters.Add("@ID", 0 , dbType: DbType.Int32, direction: ParameterDirection.Output);

                connection.Execute("dbo.spPrizes_Insert", prizeParameters, commandType: CommandType.StoredProcedure);

                model.ID = prizeParameters.Get<int>("@ID");

                return model;
            }
        }


        /// <summary>
        /// saves a new person to the database
        /// </summary>
        public PersonModel CreatePerson(PersonModel model)
        {
            using (IDbConnection connection =
                new System.Data.SqlClient.SqlConnection(GlobalConfig.GetConnectionString("Tournaments")))
            {
                var personParameters = new DynamicParameters();

                personParameters.Add("@FirstName", model.FirstName);
                personParameters.Add("@LastName", model.LastName);
                personParameters.Add("@EmailAddress", model.EmailAddress);
                personParameters.Add("@CellphoneNumber", model.CallphoneNumber);

                connection.Execute("dbo.spPeople_Insert", personParameters, commandType: CommandType.StoredProcedure);

                return model;
            }
        }


        /// <summary>
        /// returns a new list of sotred players
        /// </summary>
        public List<PersonModel> GetAllPerson()
        {
            List<PersonModel> returnedMembers;

            using (IDbConnection connection =
                new System.Data.SqlClient.SqlConnection(GlobalConfig.GetConnectionString("Tournaments")))
            {
                returnedMembers = connection.Query<PersonModel>("dbo.spPeople_GetAll").ToList();

                return returnedMembers;
            }
        }


        /// <summary>
        /// saves a new team to the database
        /// </summary>
        public TeamModel CreateTeam(TeamModel model)
        {
            using (IDbConnection connection =
                new System.Data.SqlClient.SqlConnection(GlobalConfig.GetConnectionString("Tournaments")))
            {
                var teamParameters = new DynamicParameters();

                teamParameters.Add("@TeamName", model.TeamName);
                teamParameters.Add("@ID", 0, dbType: DbType.Int32, direction: ParameterDirection.Output);

                connection.Execute("dbo.spTeams_Insert", teamParameters, commandType: CommandType.StoredProcedure);

                model.ID = teamParameters.Get<int>("@ID");

                foreach (PersonModel member in model.TeamMembers)
                {
                    var memberParameters = new DynamicParameters();

                    memberParameters.Add("@TeamID", model.ID);
                    memberParameters.Add("@PersonID", member.ID);

                    connection.Execute("dbo.spTeamMembers_Insert", memberParameters, commandType: CommandType.StoredProcedure);
                }


                return model;
            }
        }


        /// <summary>
        /// returns a new list of sotred teams
        /// </summary>
        public List<TeamModel> GetAllTeams()
        {
            List<TeamModel> output;

            using (IDbConnection connection =
                new System.Data.SqlClient.SqlConnection(GlobalConfig.GetConnectionString("Tournaments")))
            {
                output = connection.Query<TeamModel>("dbo.spTeams_GetAll").ToList();

                foreach(TeamModel team in output)
                {
                    var p = new DynamicParameters();
                    p.Add("@TeamID", team.ID);

                    team.TeamMembers = 
                        connection.Query<PersonModel>("dbo.spTeamMembers_GetByTeam", p, commandType: CommandType.StoredProcedure).ToList();
                }
            }

            return output;
        }


        /// <summary>
        /// saves a new tournament to the database
        /// </summary>
        public TournamentModel CreateTournament(TournamentModel model)
        {
            using (IDbConnection connection =
                new System.Data.SqlClient.SqlConnection(GlobalConfig.GetConnectionString("Tournaments")))
            {
                SaveTournament(model, connection);

                SavePrizeList(model, connection);

                SaveTournamentEntries(model, connection);

                SaveTournamentRounds(model, connection);

            }

            return model;
        }

        private void SaveTournamentRounds(TournamentModel model, IDbConnection connection)
        {
            // loop through the rounds
            foreach(List<MatchupModel> round in model.Rounds)
            {
                // loop through the matchups
                foreach (MatchupModel matchup in round)
                {
                    // save current matchup to the database
                    var p = new DynamicParameters();

                    p.Add("@TournamentID", model.ID);
                    p.Add("@MatchupRound", matchup.MatchupRound);
                    p.Add("@ID", 0, dbType: DbType.Int32, direction: ParameterDirection.Output);

                    connection.Execute("dbo.spMatchups_Insert", p, commandType: CommandType.StoredProcedure);
                                        
                    matchup.ID = p.Get<int>("@ID");

                    // loop through current matchup entires and save them
                    foreach(MatchupEntryModel entry in matchup.Entries)
                    {
                        p = new DynamicParameters();

                        p.Add("@MatchupID", matchup.ID);

                        if(entry.ParenMatchup == null)
                            p.Add("@ParentMatchupID", null);
                        else
                            p.Add("@ParentMatchupID", entry.ParenMatchup.ID);

                        if (entry.TeamCompeting == null)
                            p.Add("@TeamCompetingID", null);
                        else
                            p.Add("@TeamCompetingID", entry.TeamCompeting.ID);

                        p.Add("@ID", 0, dbType: DbType.Int32, direction: ParameterDirection.Output);

                        connection.Execute("dbo.spMatchupsEntry_Insert", p, commandType: CommandType.StoredProcedure);

                        entry.ID = p.Get<int>("@ID");
                    }
                }
            }
        }

        private void SaveTournament(TournamentModel model, IDbConnection connection)
        {
            var p = new DynamicParameters();
            p.Add("@TournamentName", model.TournamentName);
            p.Add("@EntryFee", model.EntryFee);
            p.Add("@ID", 0, dbType: DbType.Int32, direction: ParameterDirection.Output);

            connection.Execute("dbo.spTournament_Insert",
                p, commandType: CommandType.StoredProcedure);

            model.ID = p.Get<int>("@ID");
        }

        private void SavePrizeList(TournamentModel model, IDbConnection connection)
        {
            foreach (PrizeModel pm in model.Prizes)
            {
                var p = new DynamicParameters();

                p.Add("@TournamentID", model.ID);
                p.Add("@PrizeID", pm.ID);
                p.Add("@ID", 0, dbType: DbType.Int32, direction: ParameterDirection.Output);

                connection.Execute("dbo.spTournamentPrizes_Insert",
                p, commandType: CommandType.StoredProcedure);
            }
        }

        private void SaveTournamentEntries(TournamentModel model, IDbConnection connection)
        {
            foreach (TeamModel tm in model.EnteredTeams)
            {
                var p = new DynamicParameters();

                p.Add("@TournamentID", model.ID);
                p.Add("@TeamID", tm.ID);
                p.Add("@ID", 0, dbType: DbType.Int32, direction: ParameterDirection.Output);

                connection.Execute("dbo.spTournamentEntires_Insert",
                p, commandType: CommandType.StoredProcedure);
            }
        }

        public List<TournamentModel> GetAllTournaments()
        {
            List<TournamentModel> returnedTournaments;

            using (IDbConnection connection =
                new System.Data.SqlClient.SqlConnection(GlobalConfig.GetConnectionString("Tournaments")))
            {
                returnedTournaments = connection.Query<TournamentModel>("dbo.spTournaments_GetAll").ToList();

                foreach(TournamentModel tm in returnedTournaments)
                {
                    var p = new DynamicParameters();

                    // fetch prizes
                    p.Add("@TournamentID", tm.ID);
                    tm.Prizes = connection.Query<PrizeModel>("dbo.spPrizes_GetByTournament", p, commandType: CommandType.StoredProcedure).ToList();

                    // fetch teams
                    tm.EnteredTeams = connection.Query<TeamModel>("dbo.spTeams_GetByTournament", p, commandType: CommandType.StoredProcedure).ToList();

                    foreach (TeamModel team in tm.EnteredTeams)
                    {
                        p = new DynamicParameters();
                        p.Add("@TeamID", team.ID);

                        team.TeamMembers =
                            connection.Query<PersonModel>("dbo.spTeamMembers_GetByTeam", p, commandType: CommandType.StoredProcedure).ToList();
                    }

                    // fetch rounds
                    p = new DynamicParameters();
                    p.Add("@TournamentID", tm.ID);

                    List<MatchupModel> matchups =
                            connection.Query<MatchupModel>("dbo.spMatchups_GetByTournament", p, commandType: CommandType.StoredProcedure).ToList();

                    foreach(MatchupModel m in matchups)
                    {
                        p = new DynamicParameters();
                        p.Add("@MatchupID", m.ID);

                        // fetch rounds 
                        m.Entries = connection.Query<MatchupEntryModel>("dbo.spMatchupEntries_GetByMatchup", p, commandType: CommandType.StoredProcedure).ToList();

                        // fetch entries for each matchup
                        List<TeamModel> allTeams = GetAllTeams();

                        // fetch winners if they are any
                        if(m.WinnerID > 0)
                            m.Winner = allTeams.Where(x => x.ID == m.WinnerID).First();

                        // fetch other teams
                        foreach(MatchupEntryModel me in m.Entries)
                        {
                            if(me.TeamCompetingID > 0)
                                me.TeamCompeting = allTeams.Where(metc => metc.ID == me.TeamCompetingID).First();

                            if(me.ParenMatchupID > 0)
                                me.ParenMatchup = matchups.Where(x => x.ID == me.ParenMatchupID).First();
                        }
                    }

                    // transfer matchups into rounds
                    List<MatchupModel> currentRow = new List<MatchupModel>();
                    int currentRound = 1;

                    foreach(MatchupModel mm in matchups)
                    {
                        if (mm.MatchupRound > currentRound)
                        {
                            tm.Rounds.Add(currentRow);
                            currentRow = new List<MatchupModel>();
                            currentRound++;
                        }

                        currentRow.Add(mm);
                    }

                    tm.Rounds.Add(currentRow);
                }

            }

            return returnedTournaments;
        }
    }
}
